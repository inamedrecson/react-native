import React, { Component } from 'react';
import { View, Image, StyleSheet, ListView, Text} from 'react-native';


export default class ListV extends Component {
  constructor() {
    super();
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(['row 1', 'row 2']),
    };
  }

  render() {
    return (
    	<View style={{justifyContent: 'center'}}>
      <ListView 
        dataSource={this.state.dataSource}
        renderRow={(rowData) => <Text>{rowData}</Text>}
      />
      </View>
    );
  }
}