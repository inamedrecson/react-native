import React, { Component } from 'react';
import { View, Image, StyleSheet } from 'react-native';


const styles = StyleSheet.create({
  stretch: {
    width: 50,
    height: 200
  }
});

export default class DisplayAnImage extends Component {
  render() {
    return (
      <View>
        <Image style = {styles.stretch} 
          source={require('../assets/1.png')}
        />
        <Image
          style={{width: 50, height: 50}}
          source={require('../assets/2.png')}
        />
      </View>
    );
  }
}