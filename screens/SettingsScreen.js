import React, { Component } from 'react';
import { Button, StyleSheet, Text, View} from 'react-native';

class SettingsScreen extends React.Component {
	render () {
		return (
			<View style= {styles.container}>
				<Button title = "Go to Profile Screen"
					onPress = { () => this.props.navigation.navigate('Profile')}
				/>				
			</View>
			);

	}
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#000',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

export default SettingsScreen;