import React, { Component } from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';

class ProfileScreen extends Component{
	render(){
		return (
			<View style={styles.container}>
			<Button title = "Go back to Setting Sceen"
				onPress = { () => this.props.navigation.navigate("Settings")}
			/>
			</View>
		);

	}

}

const styles = StyleSheet.create({

	container: {

		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: "center" 

	}



	});

export default ProfileScreen;