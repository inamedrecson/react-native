import React, { Component } from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';

class DetailsScreen extends React.Component {
    render() {
        return (
          <View style={styles.container}>
            <Button title = "Go to Home Screen"
              onPress = { () => this.props.navigation.navigate("Home")}

             /> 
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

export default DetailsScreen;
