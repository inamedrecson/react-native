import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import { createStackNavigator, createMaterialTopTabNavigator, createBottomTabNavigator} from 'react-navigation';
import HomeScreen from './screens/HomeScreen'
import DetailsScreen from './screens/DetailsScreen'
import SettingScreen from './screens/SettingsScreen'
import ProfileScreen from './screens/ProfileScreen'
import ActivityInd from './components/ActivityIndicator'
import DrawLay from './components/DrawerLayoutAndroid'
import FlatL from './components/FlatList'
import DisplayAnImage from './components/Image'
import DisplayImgBkg from './components/ImageBackground'
import KbdAvoidingView from './components/KeyboardAvoidingView'
import ListV  from './components/ListView'
import ModalExample from './components/Modal'
import PickerComp from './components/Picker'
import ProgBar from './components/ProgressBarAndroid'
import RefreshableListComp from './components/RefreshControl'
import ScrollViewComp  from './components/ScrollView'
import SectionListComp from './components/SectionList'
import SliderComp from './components/Slider'
import StatusBarComp from './components/Statusbar'
import SwitchExample from './components/Switch'
import TextComp from './components/Text'
import TextInputComp from './components/TextInput'
import Touch from './components/TouchableHighlight'
import TouchableNativeFeedBackComp  from './components/TouchableNativeFeedBack'

export default class App extends React.Component {
  render() {
    return <RootStack/>;
  }
}


const RootStack = createStackNavigator(
  {
    Home: HomeScreen,
    ActInd: ActivityInd,
    DrawLayout: DrawLay,
    FlatLizt: FlatL,
    DispImg: DisplayAnImage,
    DispImgBkg: DisplayImgBkg,
    KeyAvView: KbdAvoidingView,
    LView: ListV,
    Modal: ModalExample,
    Picker: PickerComp,
    ProgressBar: ProgBar,
    RefreshControl: RefreshableListComp,
    ScrollView: ScrollViewComp,
    SectionList: SectionListComp,
    Slider: SliderComp,
    StatusBar: StatusBarComp,
    Switch: SwitchExample,
    Text: TextComp,
    TextInput: TextInputComp,
    Touchable: Touch,
    TouchableNativeFeedBack: TouchableNativeFeedBackComp 



    
  },
  {
    initialRouteName: 'Home'
  }  
);

// const SettingsStack = createStackNavigator(

//   {

//     Settings: SettingScreen,
//     Profile: ProfileScreen

//   },

//   {
//      initialRouteName: 'Settings'

//   }
// );

// const Interface = createBottomTabNavigator(

//   {
//      Home: HomeStack, 
//      Settings: SettingsStack

//   },
//   {

//     initialRouteName: 'Home'

//   }

//   );
