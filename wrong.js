import React from 'react';
import { Modal, Button, View, Text, StyleSheet, Image, StatusBar} from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';


class LogoTitle extends React.Component {
	render()
	{
		return (

			<Image source = {require('./myicon.jpg')}

			style={{ width: 30, height: 30 }}

			/>

			);

	}

}


class HomeScreen extends React.Component {

	static navigationOptions = ({ navigation }) => {

		const params = navigation.state.params || {};

		return {
			headerTitle: <LogoTitle />,
			headerRight: (
				
				<Button onPress = { () => navigation.getParam('increaseCount')}
				title = "+1"
				color = "#fff"

			  	/>
			), 
			headerLeft: (

				<Button onPress = { () => navigation.navigate('MyModal')}
				title = "Info"
				color = "#fff"

				/>
			)

		};
	}; 

	componentDidMount(){

		this.props.navigation.setParams({increaseCount: this._increaseCount});
	};

	state = {
		count: 0
	};

	_increaseCount = () => {

		this.setState({ count: this.state.count + 1 });

	};

  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
        <Button title="Go to Details" 
		
		onPress={()=> {
			/* 1. Navigate to the Details route with params */
			this.props.navigation.navigate('Details', 

				{
					itemId: 86, 
					otherParam: 'anything you want here'
				});

			}}  
			/>
      </View>
    );
  }
}

class ModalScreen extends React.Component {
	render() {

		return (

			<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
	        <Text style={{ fontSize: 30 }}>This is a modal!</Text>
        
        <Button
          onPress={() => this.props.navigation.goBack()}
          title="Dismiss"
        />

      </View>

		);
	}
}


class DetailsScreen extends React.Component {

	static navigationOptions = ( { navigation, navigationOptions }) => 
	{

		const { params } = navigation.state;

		return {
			title: params ? params.otherParam : 'A Nest
			ed Details Screen',
			headerStyle: {
				backgroundColor: navigationOptions.headerTintColor,
			},

		};

	};

  render() {

  	const { navigation } = this.props; 
  	const itemId = navigation.getParam('itemId');
  	const otherParam = navigation.getParam('otherParam', 'some default value')

    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Details Screen</Text>
        <Text>itemId: {JSON.stringify(itemId)}</Text>
        <Text>otherParam: {JSON.stringify(otherParam)}</Text>
        <Button title = "Update the title"

         onPress ={ () => this.props.navigation.setParams({otherParam: 'Updated!'})}
         />

         <Button
         	title = "Go to Home"
         	onPress={() => this.props.navigation.navigate('Home')}
         />

         <Button

         	title = "Go back"
         	onPress={ () => this.props.navigation.goBack()}
         />
  
      </View>

    );
  }
}

class CharScreen extends React.Component {

	static navigationOptions = { 
		title: 'Profile'
	}


	render() {



		return(

		<View style = {{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
			<Text>This is a CharScreen</Text>
			<Button title = "Go to home " onPress = { () => this.props.navigation.popToTop()} />
		</View>
			);

	}


}

class SettingsScreen extends React.Component {


	static navigationOptions = { 
		title: 'Settings'
	}


	render() {

		return(

		<View style = {{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
			<Text>This is a SettingsScreen</Text>
			<Button title = "Go to home " onPress = { () => this.props.navigation.navigate('Profile')} />
		</View>
			);

	}


}

class ProfileScreen extends React.Component {

	static navigationOptions = { 
		title: 'Profile'
	}

	render() {

		return(

		<View style = {{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
			<Text>This is a ProfileScreen</Text>
			<Button title = "Go to home " onPress = { () => this.props.navigation.goBack()} />
		</View>
			);

	}


}




const RootStack = createStackNavigator(
		{
 			 Home: HomeScreen,
		  Details: DetailsScreen,
		  Char: CharScreen,
		  Main: MainStack,
		  MyModal: ModalScreen,
		},
		
		{
		    initialRouteName: 'Home',
		    mode: 'modal',
			headerMode: 'none',
		    navigationOptions: {
			    
			    headerStyle: { backgroundColor: '#f4511e'},
				headerTintColor: '#fff', 
				headerTitleStyle:
				{
					fontWeight: 'bold'
				}

		}
		
		}

);

const MainStack = createStackNavigator(
	{
		Home: HomeScreen,
		Details: DetailsScreen,
		
	},

	{

		initialRouteName: 'Home',
		    navigationOptions: {
			    
			    headerStyle: { backgroundColor: '#f4511e'},
				headerTintColor: '#fff', 
				headerTitleStyle:
				{
					fontWeight: 'bold'
				}
		}

	}
);



//Create niya ang HomeStack
const HomeStack = createStackNavigator(
{
  Home: HomeScreen,
  Details: DetailsScreen,
},

{

  	initialRouteName: 'Home',
  	navigationOptions: {
			    
			    headerStyle: { backgroundColor: '#f4511e'},
				headerTintColor: '#fff', 
				headerTitleStyle:
				{
					fontWeight: 'bold'
				}

	}
}

);

//Create niya ang settings na stack
const SettingsStack = createStackNavigator(
{
  Settings: SettingsScreen,
  Profile: ProfileScreen,
},

{

  	initialRouteName: 'Settings',
  	navigationOptions: {
			    
			    headerStyle: { backgroundColor: '#f4511e'},
				headerTintColor: '#fff', 
				headerTitleStyle:
				{
					fontWeight: 'bold'
				}

	}
}

);


// Magbuhat siya ug isa ka tab na i-insert niya ang duha ka stack navigators
const TabNavigator = createBottomTabNavigator(
  {
    Home: HomeStack,
    Settings: SettingsStack,
  },

  {

  	initialRouteName: 'Home',

  }

);

export default class App extends React.Component {
  render() {
    return <RootStack/>;
  }
}
